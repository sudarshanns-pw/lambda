import json
import os
import boto3
import requests

def lambda_handler(event, context):
    
    print(json.dumps(event))
    
    status = event['detail']['status']
    env_type = event['detail']['userMetadata']['env_type']
    #env_type = 'nonprod'
    
    video_id = event['detail']['userMetadata']['video_id']
    meta_video_id = event['detail']['userMetadata']['meta_video_id']
    endpoint = os.environ.get(env_type)
    cf_url = os.environ.get('cf_url')

    video_tag = 'non-reels'
    if event['detail']['userMetadata'].get('video_tag'): 
        video_tag = event['detail']['userMetadata']['video_tag']
    
    print(video_tag)

    if video_tag == 'reels' and env_type == 'prod':
        cf_url =  os.environ.get('reels_cf_url_prod')

    if video_tag == 'reels' and env_type != 'prod':
        cf_url =  os.environ.get('reels_cf_url')    
    
    print(endpoint)
    print(cf_url)
    
    mp4_urls = {}
    hls_buildUrl = ''
    hls_file_relative_path = ''
    if status == 'COMPLETE':
    
        for i in event['detail']['outputGroupDetails']:
            output_file_group = i['type']
            print(output_file_group)
            
            
            if output_file_group == 'HLS_GROUP':
        
                filepath = i['playlistFilePaths'][0]
                hls_file_relative_path = filepath.split('/',3)[3]
                print(hls_file_relative_path)
                hls_buildUrl = '{}/{}'.format(cf_url,filepath.split('/',3)[3])
                print(hls_buildUrl)
            
            if output_file_group == 'FILE_GROUP':
                
                output_details = i['outputDetails']
                for j in output_details:
                    
                    s3_url = j['outputFilePaths'][0]
                    mp4_file_relative_path = s3_url.split('/',3)[3]
                    print(mp4_file_relative_path)
                    mp4_buildUrl = '{}/{}'.format(cf_url,s3_url.split('/',3)[3])
                    key = '{}p_url'.format(j['videoDetails']['heightInPx'])
                    mp4_urls[key] = mp4_buildUrl
                
                
        
        api_url = "{}/v1/videos/media-converter/callback".format(endpoint)
        headers = {"content-type": "application/json"}
        post_payload = {
            "status": 'SUCCESS',
            "data": {
                "video_id": meta_video_id,
                "cf_url": '{}'.format(hls_buildUrl),
                "hls_file_relative_path": hls_file_relative_path,
                "mp4_urls": mp4_urls
            }
        }
        
        print(json.dumps(post_payload))
        x = requests.post(api_url, json = post_payload, headers = headers)
        print(x.text)
    
    else:
        api_url = "{}/v1/videos/media-converter/callback".format(endpoint)
        headers = {"content-type": "application/json"}
        post_payload = {
            "status": 'FAILED',
            "data": {
                "video_id": meta_video_id
            }
        }
        print(post_payload)
        x = requests.post(api_url, json = post_payload, headers = headers)
        print(x.text)
    
    return {
        "statusCode": 200,
        "message": "Media Convert Job {}".format(event['detail']['status'])
    }
