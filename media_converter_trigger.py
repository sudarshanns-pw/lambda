import json
import logging
import boto3
import botocore
import os
from pymediainfo import MediaInfo
from botocore.config import Config
from botocore.client import ClientError

region = os.environ['AWS_REGION']

def get_signed_url(expires_in, bucket, obj):
    
    s3_cli = boto3.client("s3", region_name=region, config = Config(signature_version = 's3v4', s3={'addressing_style': 'virtual'}))
    presigned_url = s3_cli.generate_presigned_url('get_object', Params={'Bucket': bucket, 'Key': obj}, ExpiresIn=expires_in)
    return presigned_url

def lambda_handler(event, context):
    

    s3 = boto3.resource('s3')
    
    s3_source = 's3://{}/{}'.format(event['Records'][0]['s3']['bucket']['name'], event['Records'][0]['s3']['object']['key'])
    
    
    BUCKET_NAME = event['Records'][0]['s3']['bucket']['name']
    S3_KEY = event['Records'][0]['s3']['object']['key']


    SIGNED_URL_EXPIRATION = 300     # The number of seconds that the Signed URL is valid

    # Generate a signed URL for the uploaded asset
    signed_url = get_signed_url(SIGNED_URL_EXPIRATION, BUCKET_NAME, S3_KEY)
    # Launch MediaInfo
    print(signed_url)

    
    media_info = MediaInfo.parse(signed_url, library_file='/opt/libmediainfo.so.0')
    
    #this is in string format
    result = str(media_info.to_json())
    
    #converting string to dictionary
    final_result = json.loads(result)
    
    #looping over dictionary
    for i in final_result["tracks"]:
        if "width" in i:
            print("width is: ",i["width"])
            width = i['width']
            
        if "height" in i:
            print("Height is: ",i["height"])
            height = i['height']
            

    print(width ,'x', height)
    
    if height >= 720:
        job_template_name = 'PenPencil_HLS_720'
    elif height >= 480:
        job_template_name = 'PenPencil_HLS_480'
    elif height >= 360:
        job_template_name = 'PenPencil_HLS_360'
    elif height >= 240:
        job_template_name = 'PenPencil_HLS_240'
    else:  
        job_template_name = 'PenPencil_HLS'
    
    
    s3_client = boto3.client('s3')

    
    
    video_id = event['Records'][0]['s3']['object']['key'].split('.')[0]
    print(s3_source)
    sourceS3Basename = os.path.splitext(os.path.basename(s3_source))[0]
    
    #destinationS3 = 's3://{}/{}/hls/'.format(os.environ['DestinationBucket'], sourceS3Basename)

    metadata_response = s3_client.head_object(
        Bucket=event['Records'][0]['s3']['bucket']['name'],
        Key=event['Records'][0]['s3']['object']['key']
    )

    env_type = 'nonprod'
    if metadata_response['Metadata'].get('env-type'):
        env_type = metadata_response['Metadata']['env-type']


    video_tag = 'non-reels'
    if metadata_response['Metadata'].get('video-tag'):
        video_tag = metadata_response['Metadata']['video-tag']
        
    destination_bucket = os.environ['DestinationBucket']
    if video_tag == 'reels' and env_type == 'prod':
        destination_bucket = os.environ['ReelsDestinationBucketProd'] 
    elif video_tag == 'reels' and env_type != 'prod': 
       destination_bucket = os.environ['ReelsDestinationBucket']  
    
    mp4_destinationS3 = 's3://{}/{}/mp4/'.format(destination_bucket, sourceS3Basename)

    mediaConvertRole = os.environ['MediaConvertRole']
    region = 'ap-south-1'
    statusCode = 200
    body = {}
    
        
    if metadata_response['Metadata'].get('video-id'):
        meta_video_id = metadata_response['Metadata']['video-id']
    else:
        meta_video_id = video_id
        
    
    if metadata_response['Metadata'].get('job-priority'):
        job_priority = int(metadata_response['Metadata']['job-priority'])
    else:
        job_priority = 0
        
    
    job_queue = 'penpencil-rts-queue'
    if metadata_response['Metadata'].get('job-queue'):
        #job_queue = 'penpencil-ondemand-queue'
        
        job_queue_name = metadata_response['Metadata']['job-queue']
        if job_queue_name == 'penpencil-ondemand-queue':
            job_queue = 'Default'
        
    if video_tag == 'reels' :
        job_template_name = 'Reels_720'

    print(job_template_name) 
    
    print(env_type)
    #print(destinationS3)
    print(video_id)
    print(meta_video_id)
    print(job_priority)

    try:
        
        mc_client = boto3.client('mediaconvert', region_name=region)
        
        #https://idej2gpma.mediaconvert.ap-south-1.amazonaws.com
        if ("MEDIACONVERT_ENDPOINT" in os.environ):
            mediaconvert_endpoint = os.environ["MEDIACONVERT_ENDPOINT"]
        else:
            try:
                endpoints = mc_client.describe_endpoints()
                mediaconvert_endpoint = endpoints['Endpoints'][0]['Url']
            except Exception as e:
                print("Exception:\n", e)
                return {
                    'statusCode': 500,
                    'message': 'An error occurred (TooManyRequestsException) when calling the DescribeEndpoints operation (reached max retries: 4): Too Many Requests'
                }
        
        
        client = boto3.client('mediaconvert', region_name=region, endpoint_url=mediaconvert_endpoint, verify=True)
        
        
        hls_job_settings = {
            "Inputs": [
                {
                    "AudioSelectors": {
                      "Audio Selector 1": {
                        "Offset": 0,
                        "DefaultSelection": "DEFAULT",
                        "ProgramSelection": 1
                      }
                    },
                    "VideoSelector": {
                      "ColorSpace": "FOLLOW",
                      "Rotate": "AUTO",
                      "AlphaBehavior": "DISCARD"
                    },
                    "FilterEnable": "AUTO",
                    "PsiControl": "USE_PSI",
                    "FilterStrength": 0,
                    "DeblockFilter": "DISABLED",
                    "DenoiseFilter": "DISABLED",
                    "InputScanType": "AUTO",
                    "TimecodeSource": "ZEROBASED",
                    "FileInput": s3_source
                 }
            ],
            "OutputGroups": [
                # {
                #     "OutputGroupSettings": {
                #         "Type": "HLS_GROUP_SETTINGS",
                #         "HlsGroupSettings": {
                #             "Destination": destinationS3,
                #         }
                #     }
                # },
                {
                    "OutputGroupSettings": {
                        "Type": "FILE_GROUP_SETTINGS",
                        "FileGroupSettings": {
                            "Destination": mp4_destinationS3,
                        }
                    }
                }
            ]
        }
        
        
        
        jobMetadata = {'stackname': 'HLS_Conversion', 'video_id': video_id, 'env_type': env_type, 'meta_video_id': meta_video_id, 'video_tag': video_tag}
        
        hop_destination = [
                            {
                                'Priority': job_priority,
                                'Queue': 'Default',
                                'WaitMinutes': 30
                            }
                        ]
        
        
        # job = client.create_job(Queue=job_queue,Priority=job_priority, Role=mediaConvertRole, UserMetadata=jobMetadata, Settings=hls_job_settings, JobTemplate= job_template_name)
        #job = client.create_job(Role=mediaConvertRole, UserMetadata=jobMetadata, Settings=hls_job_settings, JobTemplate= job_template_name)
        
        job = client.create_job(HopDestinations=hop_destination,Queue=job_queue,Priority=job_priority, Role=mediaConvertRole, UserMetadata=jobMetadata, Settings=hls_job_settings, JobTemplate= job_template_name)
        
    except Exception as e:
        print ('Exception: %s' % e)
        statusCode = 500
        return {
            'statusCode': 500,
            'message': 'Media convert job creation .' 
        }

    finally:
        return {
            'statusCode': 200,
            'message': 'Media convert job started.' 
        }
